package ua.dp.krotov.model;

import javax.persistence.*;

/**
 * Created by E.Krotov on 27.04.2016. (e.krotov@hotmail.com))
 */
@Entity
@Table(name = "IMAGE")
public class Image {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "URL")
    private String url;

    public Image() {
    }

    public Image(String url) {
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
