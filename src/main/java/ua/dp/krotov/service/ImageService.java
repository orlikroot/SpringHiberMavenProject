package ua.dp.krotov.service;

import ua.dp.krotov.model.Image;

/**
 * Created by E.Krotov on 29.04.2016. (e.krotov@hotmail.com))
 */
public interface ImageService {
    void persistImage(Image image);
    void deleteImage(Image image);
}
